package model;

public class King extends Piece {

    public King(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 1, true, true, true, true);
    }

}

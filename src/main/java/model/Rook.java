package model;

public class Rook extends Piece {

    public Rook(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 7, false, true, true, true);
    }
}

package model;

import java.util.ArrayList;
import java.util.Collection;

public class Horse extends Piece {

    public Horse(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 2.5f, false, true, true, true);
    }

    @Override
    protected Collection<? extends String> getForwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        possibleMoves.addAll(getRightSideForwardVerticalMoves());
        possibleMoves.addAll(getLeftSideForwardVerticalMoves());
        return possibleMoves;
    }

    @Override
    protected Collection<? extends String> getBackwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        possibleMoves.addAll(getRightSideBackwardVerticalMoves());
        possibleMoves.addAll(getLeftSideBackwardVerticalMoves());
        return possibleMoves;
    }

    @Override
    protected ArrayList<String> getRightSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        possibleMoves.addAll(getForwardRightSideHorizontalMoves());
        possibleMoves.addAll(getBackwardRightSideHorizontalMoves());
        return possibleMoves;
    }

    @Override
    protected Collection<? extends String> getLeftSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        possibleMoves.addAll(getForwardLeftSideHorizontalMoves());
        possibleMoves.addAll(getBackwardLeftSideHorizontalMoves());
        return possibleMoves;
    }

    private Collection<? extends String> getBackwardRightSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'H' && currentHorizontalLocation != 'G' && currentVerticalLocation != 1)) {
            Integer newVerticalLocation = currentVerticalLocation - 1;
            char newHorizontalLocation = (char) (currentHorizontalLocation + 2);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;

    }

    private Collection<? extends String> getBackwardLeftSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'A' && currentHorizontalLocation != 'B' && currentVerticalLocation != 1)) {
            Integer newVerticalLocation = currentVerticalLocation - 1;
            char newHorizontalLocation = (char) (currentHorizontalLocation - 2);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;

    }

    private Collection<? extends String> getForwardRightSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'H' && currentHorizontalLocation != 'G' && currentVerticalLocation != 8)) {
            Integer newVerticalLocation = currentVerticalLocation + 1;
            char newHorizontalLocation = (char) (currentHorizontalLocation + 2);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }

    private Collection<? extends String> getForwardLeftSideHorizontalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'A' && currentHorizontalLocation != 'B' && currentVerticalLocation != 8)) {
            Integer newVerticalLocation = currentVerticalLocation + 1;
            char newHorizontalLocation = (char) (currentHorizontalLocation - 2);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }

    private Collection<? extends String> getLeftSideForwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'A' && currentVerticalLocation <= 6)) {
            Integer newVerticalLocation = currentVerticalLocation + 2;
            char newHorizontalLocation = (char) (currentHorizontalLocation - 1);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }

    private Collection<? extends String> getLeftSideBackwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'A' && currentVerticalLocation >= 3)) {
            Integer newVerticalLocation = currentVerticalLocation - 2;
            char newHorizontalLocation = (char) (currentHorizontalLocation - 1);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }

    private Collection<? extends String> getRightSideForwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'H' && currentVerticalLocation <= 6)) {
            Integer newVerticalLocation = currentVerticalLocation + 2;
            char newHorizontalLocation = (char) (currentHorizontalLocation + 1);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }

    private Collection<? extends String> getRightSideBackwardVerticalMoves() {
        ArrayList<String> possibleMoves = new ArrayList<>();
        float moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentHorizontalLocation != 'H' && currentVerticalLocation >= 3)) {
            Integer newVerticalLocation = currentVerticalLocation - 2;
            char newHorizontalLocation = (char) (currentHorizontalLocation + 1);
            possibleMoves.add((newHorizontalLocation) + String.valueOf(newVerticalLocation));
            currentHorizontalLocation = newHorizontalLocation;
            currentVerticalLocation = newVerticalLocation;
            moves += 2.5;
        }
        return possibleMoves;
    }
}

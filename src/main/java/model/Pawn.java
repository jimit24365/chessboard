package model;

public class Pawn extends Piece {

    public Pawn(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 1, false, false, true, false);
    }
}

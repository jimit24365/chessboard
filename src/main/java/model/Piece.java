package model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Defines basic unit of chess bord game piece.
 */
public abstract class Piece {

    /**
     * Defines number of steps piece can perform at a time. If steps are decimal value than particular direction will be changed (Special case for horse).
     */
    private float moveThreshold;

    /**
     * Defines the ability of piece to move vertically on board
     */
    private boolean canMoveVertically;

    /**
     * Defines the ability of piece to move horizontally on board
     */
    private boolean canMoveHorizontally;

    /**
     * Defines the ability of piece to move diagonally on board
     */
    private boolean canMoveDiagonally;

    /**
     * Defines the vertical location of piece on the board (possible values are (1 to 8))
     */
    private Integer verticalLocation;

    /**
     * Defines the vertical location of piece on the board (possible values are (A to H))
     */
    private char horizontalLocation;

    /**
     * Defines the ability of piece to move backward
     */
    private boolean canMoveBackward;

    Piece(Integer verticalLocation, char horizontalLocation, float moveThreshold, boolean canMoveDiagonally, boolean canMoveHorizontally, boolean canMoveVertically, boolean canMoveBackward) {
        this.moveThreshold = moveThreshold;
        this.canMoveVertically = canMoveVertically;
        this.canMoveHorizontally = canMoveHorizontally;
        this.canMoveDiagonally = canMoveDiagonally;
        this.verticalLocation = verticalLocation;
        this.horizontalLocation = horizontalLocation;
        this.canMoveBackward = canMoveBackward;

        if (verticalLocation > 8 || verticalLocation < 1) {
            throw new IllegalArgumentException("Specified location vertical of piece cannot be outside board");
        }

        if (moveThreshold > 7 || moveThreshold < 1) {
            throw new IllegalArgumentException("Specified threshold for piece cannot be applied");
        }

        if (horizontalLocation < 'A' || horizontalLocation > 'H') {
            throw new IllegalArgumentException("Specified location horizontal of piece cannot be outside board");
        }
    }

    /**
     * Defines possible moves from the piece at given point of time.
     * @return
     */
    protected float getMoveThreshold() {
        return moveThreshold;
    }

    /**
     * Defines whether piece can move vertically or not
     * @return
     */
    protected boolean canMoveVertically() {
        return canMoveVertically;
    }

    /**
     * Defines whether piece can move horizontally or not
     * @return
     */
    protected boolean canMoveHorizontally() {
        return canMoveHorizontally;
    }

    /**
     * Defines whether piece can move diagonally or not
     * @return
     */
    protected boolean canMoveDiagonally() {
        return canMoveDiagonally;
    }

    /**
     * Returns the vertical location of the piece
     * @return
     */
    protected Integer getVerticalLocation() {
        return verticalLocation;
    }

    /**
     * Returns the horizontal location of the piece
     * @return
     */
    protected char getHorizontalLocation() {
        return horizontalLocation;
    }

    /**
     * Defines whether piece can move backward or not
     * @return
     */
    protected boolean canMoveBackward() {
        return canMoveBackward;
    }

    /**
     * Calculates all possible moves based on parameters of piece
     *
     * @return List of possible locations.
     */
    public ArrayList<String> getPossibleMovements() {
        ArrayList<String> possibleMoveList = new ArrayList<>();
        if (canMoveHorizontally()) {
            possibleMoveList.addAll(getRightSideHorizontalMoves());
            possibleMoveList.addAll(getLeftSideHorizontalMoves());
        }
        if (canMoveVertically()) {
            possibleMoveList.addAll(getForwardVerticalMoves());
            if (canMoveBackward()) {
                possibleMoveList.addAll(getBackwardVerticalMoves());
            }
        }
        if (canMoveDiagonally()) {
            possibleMoveList.addAll(getRightForwardDiagonalMoves());
            possibleMoveList.addAll(getLeftForwardDiagonalMoves());
            if (canMoveBackward()) {
                possibleMoveList.addAll(getLeftBackwardDiagonalMoves());
                possibleMoveList.addAll(getRightBackwardDiagonalMoves());
            }
        }
        return possibleMoveList;
    }

    /**
     * Calculates All horizontal moves on the right side of current location
     * @return List of string representing possible moves
     */
    protected ArrayList<String> getRightSideHorizontalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        char currentHorizontalPosition = getHorizontalLocation();
        while (moves < getMoveThreshold() && currentHorizontalPosition != 'H') {
            char newHorizontalPosition = (char) (currentHorizontalPosition + 1);
            currentPossibleMoves.add((newHorizontalPosition) + getVerticalLocation().toString());
            currentHorizontalPosition = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All horizontal moves on the left side of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getLeftSideHorizontalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        char currentHorizontalPosition = getHorizontalLocation();
        while (moves < getMoveThreshold() && currentHorizontalPosition != 'A') {
            char newHorizontalPosition = (char) (currentHorizontalPosition - 1);
            currentPossibleMoves.add((newHorizontalPosition) + getVerticalLocation().toString());
            currentHorizontalPosition = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All vertical moves on the forward side of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getForwardVerticalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        while (moves < getMoveThreshold() && currentVerticalLocation != 8) {
            Integer newVerticalLocation = currentVerticalLocation + 1;
            currentPossibleMoves.add((getHorizontalLocation()) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All vertical moves on the backward side of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getBackwardVerticalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        while (moves < getMoveThreshold() && currentVerticalLocation != 1) {
            Integer newVerticalLocation = currentVerticalLocation - 1;
            currentPossibleMoves.add((getHorizontalLocation()) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All diagonal moves on the left side forward of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getLeftForwardDiagonalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentVerticalLocation != 8 && currentHorizontalLocation != 'A')) {
            Integer newVerticalLocation = currentVerticalLocation + 1;
            char newHorizontalPosition = (char) (currentHorizontalLocation - 1);
            currentPossibleMoves.add((newHorizontalPosition) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            currentHorizontalLocation = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All diagonal moves on the right side forward of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getRightForwardDiagonalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentVerticalLocation != 8 && currentHorizontalLocation != 'H')) {
            Integer newVerticalLocation = currentVerticalLocation + 1;
            char newHorizontalPosition = (char) (currentHorizontalLocation + 1);
            currentPossibleMoves.add((newHorizontalPosition) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            currentHorizontalLocation = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;
    }

    /**
     * Calculates All diagonal moves on the left side backward of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getLeftBackwardDiagonalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentVerticalLocation != 1 && currentHorizontalLocation != 'A')) {
            Integer newVerticalLocation = currentVerticalLocation - 1;
            char newHorizontalPosition = (char) (currentHorizontalLocation - 1);
            currentPossibleMoves.add((newHorizontalPosition) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            currentHorizontalLocation = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;

    }

    /**
     * Calculates All diagonal moves on the right side backward of current location
     * @return List of string representing possible moves
     */
    protected Collection<? extends String> getRightBackwardDiagonalMoves() {
        ArrayList<String> currentPossibleMoves = new ArrayList<>();
        int moves = 0;
        Integer currentVerticalLocation = getVerticalLocation();
        char currentHorizontalLocation = getHorizontalLocation();
        while (moves < getMoveThreshold() && (currentVerticalLocation != 1 && currentHorizontalLocation != 'H')) {
            Integer newVerticalLocation = currentVerticalLocation - 1;
            char newHorizontalPosition = (char) (currentHorizontalLocation + 1);
            currentPossibleMoves.add((newHorizontalPosition) + String.valueOf(newVerticalLocation));
            currentVerticalLocation = newVerticalLocation;
            currentHorizontalLocation = newHorizontalPosition;
            moves++;
        }
        return currentPossibleMoves;
    }

}

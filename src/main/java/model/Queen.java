package model;

public class Queen extends Piece {

    public Queen(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 7, true, true, true, true);
    }

}

package model;

public class Bishop extends Piece {

    public Bishop(int verticalLocation, char horizontalLocation) {
        super(verticalLocation, horizontalLocation, 7, true, false, false, true);
    }

}

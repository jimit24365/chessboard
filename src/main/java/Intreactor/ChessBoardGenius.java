package Intreactor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import model.Bishop;
import model.Horse;
import model.King;
import model.Pawn;
import model.Piece;
import model.Queen;
import model.Rook;

public class ChessBoardGenius {

    private static String GENERIC_ERROR_MSG = "Input should be in following format <Chess Piece><Space><Location>";

    public static void main(String[] arg) {
        System.out.print("Enter Your Value: ");
        Scanner inputScanner = new Scanner(System.in);
        String userInput = inputScanner.nextLine();
        String[] wordsList = userInput.split(" ");
        if (wordsList.length != 2) {
            System.out.println(GENERIC_ERROR_MSG);
            return;
        }
        if (wordsList[1].length() != 2) {
            System.out.println("Invalid location for given piece");
            return;
        }
        char horizontalLocation = Character.toUpperCase(wordsList[1].charAt(0));
        int verticalLocation = Character.getNumericValue(wordsList[1].charAt(1));
        Piece piece = null;
        try {

            switch (wordsList[0].toUpperCase()) {
                case "KING":
                    piece = new King(verticalLocation, horizontalLocation);
                    break;
                case "QUEEN":
                    piece = new Queen(verticalLocation, horizontalLocation);
                    break;
                case "HORSE":
                    piece = new Horse(verticalLocation, horizontalLocation);
                    break;
                case "ROOK":
                    piece = new Rook(verticalLocation, horizontalLocation);
                    break;
                case "BISHOP":
                    piece = new Bishop(verticalLocation, horizontalLocation);
                    break;
                case "PAWN":
                    piece = new Pawn(verticalLocation, horizontalLocation);
                    break;
                default:
                    System.out.println();
                    break;

            }
        } catch (IllegalArgumentException e) {
            System.out.println(GENERIC_ERROR_MSG);
        }
        ArrayList<String> moves = piece.getPossibleMovements();
        Collections.sort(moves);
        String outPut = moves.toString().replace("[", "");
        outPut = outPut.replace("]", "");
        System.out.println("Possible Values : " + outPut);
    }
}

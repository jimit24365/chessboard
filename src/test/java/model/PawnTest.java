package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PawnTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Pawn pawn = new Pawn(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Pawn pawn = new Pawn(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        Pawn pawn = new Pawn(4, 'H');
        List<String> expectedValues = Arrays.asList("H5");
        ArrayList<String> actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        pawn = new Pawn(8, 'A');
        expectedValues = Arrays.asList();
        actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        pawn = new Pawn(1, 'A');
        expectedValues = Arrays.asList("A2");
        actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        pawn = new Pawn(1, 'H');
        expectedValues = Arrays.asList("H2");
        actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        pawn = new Pawn(8, 'H');
        expectedValues = Arrays.asList();
        actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        pawn = new Pawn(3, 'E');
        expectedValues = Arrays.asList("E4");
        actualValues = pawn.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));
    }
}

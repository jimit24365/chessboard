package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HorseTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Horse horse = new Horse(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Horse horse = new Horse(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        Horse horse = new Horse(4, 'H');
        List<String> expectedValues = Arrays.asList("F3", "F5", "G2", "G6");
        ArrayList<String> actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        horse = new Horse(8, 'A');
        expectedValues = Arrays.asList("B6", "C7");
        actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        horse = new Horse(1, 'A');
        expectedValues = Arrays.asList("B3", "C2");
        actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        horse = new Horse(1, 'H');
        expectedValues = Arrays.asList("F2", "G3");
        actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        horse = new Horse(8, 'H');
        expectedValues = Arrays.asList("F7", "G6");
        actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        horse = new Horse(3, 'E');
        expectedValues = Arrays.asList("C2", "C4", "D1", "D5", "F1", "F5", "G2", "G4");
        actualValues = horse.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

    }
}

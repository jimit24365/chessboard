package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BishopTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Bishop bishop = new Bishop(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Bishop bishop = new Bishop(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        Bishop bishop = new Bishop(4, 'H');
        List<String> expectedValues = Arrays.asList("D8", "E1", "E7", "F2", "F6", "G3", "G5");
        ArrayList<String> actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        bishop = new Bishop(8, 'A');
        expectedValues = Arrays.asList("B7", "C6", "D5", "E4", "F3", "G2", "H1");
        actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        bishop = new Bishop(1, 'A');
        expectedValues = Arrays.asList("B2", "C3", "D4", "E5", "F6", "G7", "H8");
        actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        bishop = new Bishop(1, 'H');
        expectedValues = Arrays.asList("A8", "B7", "C6", "D5", "E4", "F3", "G2");
        actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        bishop = new Bishop(8, 'H');
        expectedValues = Arrays.asList("A1", "B2", "C3", "D4", "E5", "F6", "G7");
        actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        bishop = new Bishop(3, 'E');
        expectedValues = Arrays.asList("A7", "B6", "C1", "C5", "D2", "D4", "F2", "F4", "G1", "G5", "H6");
        actualValues = bishop.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));
    }
}

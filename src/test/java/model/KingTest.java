package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KingTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Piece piece = new King(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Piece piece = new King(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        King king = new King(4, 'H');
        List<String> expectedValues = Arrays.asList("G4", "H5", "H3", "G5", "G3");
        ArrayList<String> actualValues = king.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));
    }
}

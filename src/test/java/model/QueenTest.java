package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueenTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Queen queen = new Queen(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Queen queen = new Queen(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        Queen queen = new Queen(4, 'H');
        List<String> expectedValues = Arrays.asList("A4", "B4", "C4", "D4", "D8", "E1", "E4", "E7", "F2", "F4", "F6", "G3", "G4", "G5", "H1", "H2", "H3", "H5", "H6", "H7", "H8");
        ArrayList<String> actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        queen = new Queen(8, 'A');
        expectedValues = Arrays.asList("A1", "A2", "A3", "A4", "A5", "A6", "A7", "B7", "B8", "C6", "C8", "D5", "D8", "E4", "E8", "F3", "F8", "G2", "G8", "H1", "H8");
        actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        queen = new Queen(1, 'A');
        expectedValues = Arrays.asList("A2", "A3", "A4", "A5", "A6", "A7", "A8", "B1", "B2", "C1", "C3", "D1", "D4", "E1", "E5", "F1", "F6", "G1", "G7", "H1", "H8");
        actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        queen = new Queen(1, 'H');
        expectedValues = Arrays.asList("A1", "A8", "B1", "B7", "C1", "C6", "D1", "D5", "E1", "E4", "F1", "F3", "G1", "G2", "H2", "H3", "H4", "H5", "H6", "H7", "H8");
        actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        queen = new Queen(8, 'H');
        expectedValues = Arrays.asList("A1", "A8", "B2", "B8", "C3", "C8", "D4", "D8", "E5", "E8", "F6", "F8", "G7", "G8", "H1", "H2", "H3", "H4", "H5", "H6", "H7");
        actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        queen = new Queen(3, 'E');
        expectedValues = Arrays.asList("A3", "A7", "B3", "B6", "C1", "C3", "C5", "D2", "D3", "D4", "E1", "E2", "E4", "E5", "E6", "E7", "E8", "F2", "F3", "F4", "G1", "G3", "G5", "H3", "H6");
        actualValues = queen.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));
    }
}

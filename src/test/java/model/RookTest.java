package model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RookTest {

    @Test
    public void testWithInvalidHorizontalLocation() {
        try {
            Rook rook = new Rook(1, 'I');
            Assert.fail("Piece class should throw exception for invalid horizontal location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location horizontal of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithInvalidVerticalLocation() {
        try {
            Rook rook = new Rook(0, 'H');
            Assert.fail("Piece class should throw exception for invalid vertical location");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Specified location vertical of piece cannot be outside board", e.getMessage());
        }
    }

    @Test
    public void testWithValidArguments() {
        Rook rook = new Rook(4, 'H');
        List<String> expectedValues = Arrays.asList("A4", "B4", "C4", "D4", "E4", "F4", "G4", "H1", "H2", "H3", "H5", "H6", "H7", "H8");
        ArrayList<String> actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        rook = new Rook(8, 'A');
        expectedValues = Arrays.asList("A1", "A2", "A3", "A4", "A5", "A6", "A7", "B8", "C8", "D8", "E8", "F8", "G8", "H8");
        actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        rook = new Rook(1, 'A');
        expectedValues = Arrays.asList("A2", "A3", "A4", "A5", "A6", "A7", "A8", "B1", "C1", "D1", "E1", "F1", "G1", "H1");
        actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        rook = new Rook(1, 'H');
        expectedValues = Arrays.asList("A1", "B1", "C1", "D1", "E1", "F1", "G1", "H2", "H3", "H4", "H5", "H6", "H7", "H8");
        actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        rook = new Rook(8, 'H');
        expectedValues = Arrays.asList("A8", "B8", "C8", "D8", "E8", "F8", "G8", "H1", "H2", "H3", "H4", "H5", "H6", "H7");
        actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));

        rook = new Rook(3, 'E');
        expectedValues = Arrays.asList("A3", "B3", "C3", "D3", "E1", "E2", "E4", "E5", "E6", "E7", "E8", "F3", "G3", "H3");
        actualValues = rook.getPossibleMovements();
        Assert.assertTrue("Possible movements should be generated with valid inputs", actualValues.containsAll(expectedValues) && expectedValues.containsAll(actualValues));
    }
}
